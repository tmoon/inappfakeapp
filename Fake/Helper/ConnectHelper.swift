//
//  ConnectHelper.swift
//  Fake
//
//  Created by HDWebSoft 2016 on 1/4/20.
//  Copyright © 2020 NatsuSalamada. All rights reserved.
//

import UIKit
import ConnectFramework
class ConnectHelper: NSObject {
    
    class func showPurchaseController(inViewController: UIViewController, completion: @escaping(Bool) -> Void ) {
        let v = PurchaseViewController.createPurchaseViewController(resultHandler:{ (didPurchase, productId) in
            if didPurchase == true,
                let idPro = productId {
                saveUserPurchased(idProduct: idPro)
            }
            completion(didPurchase)
        })
        inViewController.present(v, animated: true, completion: nil)
    }
    
    class func checkUserPurchased() -> Bool {
        return PurchaseViewController.didPurchase()
    }
    
    class func saveUserPurchased(idProduct: String) {
        UserDefaults.standard.set(true, forKey: idProduct)
        UserDefaults.standard.synchronize()
    }
}
