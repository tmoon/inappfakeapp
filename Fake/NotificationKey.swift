//
//  NotificationKey.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 18/04/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name{
    static let showSettingsLibraryOfFakeMessage              = Notification.Name("showSettingsLibraryOfFakeMessage")
    static let Diss              = Notification.Name("Diss")
    static let showSettingCellFakeMessage = Notification.Name("showSettingCellFakeMessage")
    static let reloadSeg = Notification.Name("reloadSeg")
    
     static let showSetWallpapercompelet = Notification.Name("showSetWallpapercompelet")
}
