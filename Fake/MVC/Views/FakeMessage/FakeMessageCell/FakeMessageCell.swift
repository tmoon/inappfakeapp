//
//  FakeMessageCell.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 23/04/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import UIKit

class FakeMessageCell: UICollectionViewCell {
    @IBOutlet weak var iconChat: UIImageView!
    @IBOutlet weak var typeChat: DesignableLabel!
    @IBOutlet weak var name: DesignableLabel!
    @IBOutlet weak var message: DesignableLabel!
    @IBOutlet weak var MaskBlur: UIView!
    @IBOutlet weak var timeCell: DesignableLabel!
    
}
