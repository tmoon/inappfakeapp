//
//  ChooseChatCell.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 03/05/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import UIKit

class ChooseChatCell: UITableViewCell {

    @IBOutlet weak var imageChat: UIImageView!
    @IBOutlet weak var nameChat: DesignableLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
