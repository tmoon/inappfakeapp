//
//  SegMesChatCallController.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 16/04/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import UIKit


class SegMesChatCallController: UIViewController {

    
    
    
    
    // Var Fake Message
    
    // Picker Image
    let imagePicker = UIImagePickerController()
    // Picker Image
    
    
    // Library Var
    
    
    let blackViewFakeMessageWallpaper = UIView()
    var mainViewFakeMessageWallpaper:UIView = UIView()
    
    let blackViewFakeMessageCell = UIView()
    var mainViewFakeMessageCell:UIView = UIView()
    
    // Library Var
    // Var Fake Message
    
    // Contain View Var
    
    enum TabIndex : Int {
        case FakeMessage = 0
        case FakeChat = 1
        case FakeCall = 2
    }
    
    @IBOutlet weak var Fake: UIView!
    
    
    
    // Contain View Var
    
    // ViewController Child
    var currentViewController: UIViewController?
    lazy var FakeMessageChildTabVC: UIViewController? = {
        let FakeMessageChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "FakeMessage")
        return FakeMessageChildTabVC
    }()
    lazy var FakeChatChildTabVC : UIViewController? = {
        let FakeChatChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "FakeChat")
        return FakeChatChildTabVC
    }()
    lazy var FakeCallChildTabVC : UIViewController? = {
        let FakeCallChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "FakeCall")
        return FakeCallChildTabVC
    }()
    // ViewController Child
    
    // Banner Var
    @IBOutlet weak var BannerView: UIView!
    @IBOutlet weak var heightOfBannerView: NSLayoutConstraint!
    // Banner Var
    
    // Seg Var
    @IBOutlet weak var SegControl: UISegmentedControl!
    
    @IBOutlet weak var widthSeg: NSLayoutConstraint!
    // Seg Var
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Func Of Fake Message Call
        
        // config picker Image
        self.imagePicker.delegate = self
        
        
        // Func Of Fake Message Call
        
        // Func Seg Func Call
        self.setUpSeg()
        self.registerNotification()
        // Func Seg Func Call
        
        // Func Banner Call
       self.offBanner()
        
        // Func Banner Call
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: "FakeMessage_Interstitial_Preview") || UserDefaults.standard.bool(forKey: "FakeMessage_Interstitial_Save") || UserDefaults.standard.bool(forKey: "FakeMessage_Interstitial_Active"){
            
           UserDefaults.standard.set(false, forKey: "FakeMessage_Interstitial_Preview")
            UserDefaults.standard.set(false, forKey: "FakeMessage_Interstitial_Save")
            UserDefaults.standard.set(false, forKey: "FakeMessage_Interstitial_Active")
        }
        if showSetWallpapercompeletBool{
            showSetWallpapercompelet()
            showSetWallpapercompeletBool = false
        }
        if UserDefaults.standard.value(forKey: "Purchase") != nil{
            if BannerView != nil{
               BannerView.isHidden = true
            }
            
        }
    }
    // registerNotification
    func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(showSettingsLibraryOfFakeMessage), name: .showSettingsLibraryOfFakeMessage , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showSettingCellFakeMessage), name: .showSettingCellFakeMessage , object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(Diss), name: .Diss , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showSetWallpapercompelet), name: .showSetWallpapercompelet , object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    // Seg Setup Func
    
    
    
    private func setUpSeg(){
        var font:UIFont!
        if UIScreen.main.bounds.width >= 768{
            font = UIFont(name: "SFProText-Medium", size: WIPA(w: 13))
            self.widthSeg.constant = WIPA(w: self.widthSeg.constant)
        }else{
            font = UIFont(name: "SFProText-Medium", size: WIPH(w: 13))
            self.widthSeg.constant = WIPH(w: self.widthSeg.constant)
        }
        SegControl.setTitleTextAttributes([NSAttributedStringKey.font: font],for: .normal)
        SegControl.selectedSegmentIndex = TabIndex.FakeMessage.rawValue
        displayCurrentTab(TabIndex.FakeMessage.rawValue)
    }
    // Func Of Fake Message
    
    
    func getCurrentDate()-> Date {
        var now = Date()
        var nowComponents = DateComponents()
        let calendar = Calendar.current
        nowComponents.year = Calendar.current.component(.year, from: now)
        nowComponents.month = Calendar.current.component(.month, from: now)
        nowComponents.day = Calendar.current.component(.day, from: now)
        nowComponents.hour = Calendar.current.component(.hour, from: now)
        nowComponents.minute = Calendar.current.component(.minute, from: now)
        nowComponents.second = Calendar.current.component(.second, from: now)
        nowComponents.timeZone = NSTimeZone.local
        now = calendar.date(from: nowComponents)!
        return now as Date
    }
    
    func getDayEnd(minute:Int) -> Date{
        var dateComponent = DateComponents()
        let currentDate = getCurrentDate()
        
        dateComponent.minute = minute
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        return futureDate!
    }
    
    // func Up view Library
    @objc func showSettingsLibraryOfFakeMessage(){
        if let window = UIApplication.shared.keyWindow {
            
            if UIScreen.main.bounds.width >= 768{
                blackViewFakeMessageWallpaper.backgroundColor = UIColor(displayP3Red: 216/255, green: 216/255, blue: 216/255, alpha: 0.7)
                
                blackViewFakeMessageWallpaper.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
                
                window.addSubview(blackViewFakeMessageWallpaper)
                window.addSubview(mainViewFakeMessageWallpaper)
                mainViewFakeMessageWallpaper.alpha = 0
                blackViewFakeMessageWallpaper.frame = window.frame
                blackViewFakeMessageWallpaper.alpha = 0
                mainViewFakeMessageWallpaper.backgroundColor = UIColor.clear
                
                let height: CGFloat = 218
                let y = window.frame.height - height - 195
                mainViewFakeMessageWallpaper.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
                let wi = mainViewFakeMessageWallpaper.frame.width - 408
                
                
                let mid = (mainViewFakeMessageWallpaper.frame.width - wi)/2
                
                
                
                let changeBackgroundView = UIView(frame: CGRect(x: mid, y: 0, width: wi, height: 44))
                
                changeBackgroundView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageWallpaper.addSubview(changeBackgroundView)
                changeBackgroundView.clipsToBounds = true
                
                let changeBackgroundTilte = UILabel(frame: changeBackgroundView.bounds)
                changeBackgroundTilte.font = UIFont(name: "SFProText-Regular", size: WIPA(w: 15))
                changeBackgroundTilte.textAlignment = .center
                changeBackgroundTilte.textColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 0/255, alpha: 0.4)
                changeBackgroundTilte.text = "Preview & Change Wallpaper"
                
                let blurEffect = UIBlurEffect(style: .extraLight)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame = changeBackgroundView.bounds
                
                blurEffectView.layer.masksToBounds = false
                blurEffectView.alpha = 1
                
                changeBackgroundView.addSubview(blurEffectView)
                changeBackgroundView.addSubview(changeBackgroundTilte)
                
                changeBackgroundView.roundCorners([.topLeft,.topRight], radius: 13)
                
                
                
                let previewView = UIView(frame: CGRect(x: mid, y: 45, width: wi, height: 57))
                
                previewView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageWallpaper.addSubview(previewView)
                
                let previewButton = UIButton(frame: previewView.bounds)
                previewButton.setTitle("Preview", for: .normal)
                previewButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPA(w: 22))
                previewButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                previewButton.addTarget(self, action: #selector(SegMesChatCallController.previewActionUp(_:)), for: .touchUpInside)
                previewButton.addTarget(self, action: #selector(SegMesChatCallController.previewActionIn(_:)), for: .touchDown)
                previewButton.addTarget(self, action: #selector(SegMesChatCallController.previewActionExit(_:)), for: .touchUpOutside)
                
                let blurEffectViewpreviewView = UIVisualEffectView(effect: blurEffect)
                blurEffectViewpreviewView.frame = previewView.bounds
                
                blurEffectViewpreviewView.layer.masksToBounds = false
                blurEffectViewpreviewView.alpha = 1
                previewView.addSubview(blurEffectViewpreviewView)
                previewView.addSubview(previewButton)
                
                
                
                
                
                
                
                
                let defaultView = UIView(frame: CGRect(x: mid, y: 103, width: wi, height: 57))
                
                defaultView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageWallpaper.addSubview(defaultView)
                
                let defaultButton = UIButton(frame: defaultView.bounds)
                defaultButton.setTitle("Default Wallpaper", for: .normal)
                defaultButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPA(w: 22))
                defaultButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                defaultButton.addTarget(self, action: #selector(SegMesChatCallController.defaultActionUp(_:)), for: .touchUpInside)
                defaultButton.addTarget(self, action: #selector(SegMesChatCallController.defaultActionIn(_:)), for: .touchDown)
                defaultButton.addTarget(self, action: #selector(SegMesChatCallController.defaultActionExit(_:)), for: .touchUpOutside)
                
                let blurEffectViewdefaultView = UIVisualEffectView(effect: blurEffect)
                blurEffectViewdefaultView.frame = defaultView.bounds
                
                blurEffectViewdefaultView.layer.masksToBounds = false
                blurEffectViewdefaultView.alpha = 1
                defaultView.addSubview(blurEffectViewdefaultView)
                defaultView.addSubview(defaultButton)
                
                let selectphotofromlibraryView = UIView(frame: CGRect(x: mid, y: 161, width: wi, height: 57))
                
                selectphotofromlibraryView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
                mainViewFakeMessageWallpaper.addSubview(selectphotofromlibraryView)
                selectphotofromlibraryView.roundCorners([.bottomLeft,.bottomRight], radius: 13)
                
                let selectphotofromlibraryButton = UIButton(frame: selectphotofromlibraryView.bounds)
                selectphotofromlibraryButton.setTitle("Change Wallpaper from library ", for: .normal)
                selectphotofromlibraryButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPA(w: 22))
                selectphotofromlibraryButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                selectphotofromlibraryButton.backgroundColor = UIColor.clear
                selectphotofromlibraryButton.addTarget(self, action: #selector(SegMesChatCallController.selectphotofromlibraryActionUp(_:)), for: .touchUpInside)
                selectphotofromlibraryButton.addTarget(self, action: #selector(SegMesChatCallController.selectphotofromlibraryActionIn(_:)), for: .touchDown)
                selectphotofromlibraryButton.addTarget(self, action: #selector(SegMesChatCallController.selectphotofromlibraryActionExit(_:)), for: .touchUpOutside)
                
                let blurEffectViewselectphotofromlibraryView = UIVisualEffectView(effect: blurEffect)
                blurEffectViewselectphotofromlibraryView.frame = defaultView.bounds
                
                blurEffectViewselectphotofromlibraryView.layer.masksToBounds = false
                blurEffectViewselectphotofromlibraryView.alpha = 1
                selectphotofromlibraryView.addSubview(blurEffectViewselectphotofromlibraryView)
                selectphotofromlibraryView.addSubview(selectphotofromlibraryButton)
                
                
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    
                    self.blackViewFakeMessageWallpaper.alpha = 1
                    self.mainViewFakeMessageWallpaper.alpha = 1
                    
                    self.mainViewFakeMessageWallpaper.frame = CGRect(x: 0, y: y, width: self.mainViewFakeMessageWallpaper.frame.width, height: height)
                    
                    
                }, completion: nil)
            }else{
                blackViewFakeMessageWallpaper.backgroundColor = UIColor(displayP3Red: 216/255, green: 216/255, blue: 216/255, alpha: 0.7)
                
                blackViewFakeMessageWallpaper.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
                
                window.addSubview(blackViewFakeMessageWallpaper)
                window.addSubview(mainViewFakeMessageWallpaper)
                blackViewFakeMessageWallpaper.frame = window.frame
                blackViewFakeMessageWallpaper.alpha = 0
                mainViewFakeMessageWallpaper.backgroundColor = UIColor.clear
                
                let height: CGFloat = 291
                let y = window.frame.height - height
                mainViewFakeMessageWallpaper.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
                let wi = mainViewFakeMessageWallpaper.frame.width - 16
                
                
                let mid = (mainViewFakeMessageWallpaper.frame.width - wi)/2
                
                
                
                let changeBackgroundView = UIView(frame: CGRect(x: mid, y: 0, width: wi, height: 44))
                
                changeBackgroundView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageWallpaper.addSubview(changeBackgroundView)
                changeBackgroundView.clipsToBounds = true
                
                let changeBackgroundTilte = UILabel(frame: changeBackgroundView.bounds)
                changeBackgroundTilte.font = UIFont(name: "SFProText-Regular", size: WIPH(w: 15))
                changeBackgroundTilte.textAlignment = .center
                changeBackgroundTilte.textColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 0/255, alpha: 0.4)
                changeBackgroundTilte.text = "Preview & Change Wallpaper"
                
                let blurEffect = UIBlurEffect(style: .extraLight)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame = changeBackgroundView.bounds
                
                blurEffectView.layer.masksToBounds = false
                blurEffectView.alpha = 1
                
                changeBackgroundView.addSubview(blurEffectView)
                changeBackgroundView.addSubview(changeBackgroundTilte)
                
                changeBackgroundView.roundCorners([.topLeft,.topRight], radius: 13)
                
                
                
                let previewView = UIView(frame: CGRect(x: mid, y: 45, width: wi, height: 57))
                
                previewView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageWallpaper.addSubview(previewView)
                
                let previewButton = UIButton(frame: previewView.bounds)
                previewButton.setTitle("Preview", for: .normal)
                previewButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPH(w: 22))
                previewButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                previewButton.addTarget(self, action: #selector(SegMesChatCallController.previewActionUp(_:)), for: .touchUpInside)
                previewButton.addTarget(self, action: #selector(SegMesChatCallController.previewActionIn(_:)), for: .touchDown)
                previewButton.addTarget(self, action: #selector(SegMesChatCallController.previewActionExit(_:)), for: .touchUpOutside)
                
                let blurEffectViewpreviewView = UIVisualEffectView(effect: blurEffect)
                blurEffectViewpreviewView.frame = previewView.bounds
                
                blurEffectViewpreviewView.layer.masksToBounds = false
                blurEffectViewpreviewView.alpha = 1
                previewView.addSubview(blurEffectViewpreviewView)
                previewView.addSubview(previewButton)
                
                
                
                
                
                
                
                
                let defaultView = UIView(frame: CGRect(x: mid, y: 103, width: wi, height: 57))
                
                defaultView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageWallpaper.addSubview(defaultView)
                
                let defaultButton = UIButton(frame: defaultView.bounds)
                defaultButton.setTitle("Default Wallpaper", for: .normal)
                defaultButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPH(w: 22))
                defaultButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                defaultButton.addTarget(self, action: #selector(SegMesChatCallController.defaultActionUp(_:)), for: .touchUpInside)
                defaultButton.addTarget(self, action: #selector(SegMesChatCallController.defaultActionIn(_:)), for: .touchDown)
                defaultButton.addTarget(self, action: #selector(SegMesChatCallController.defaultActionExit(_:)), for: .touchUpOutside)
                
                let blurEffectViewdefaultView = UIVisualEffectView(effect: blurEffect)
                blurEffectViewdefaultView.frame = defaultView.bounds
                
                blurEffectViewdefaultView.layer.masksToBounds = false
                blurEffectViewdefaultView.alpha = 1
                defaultView.addSubview(blurEffectViewdefaultView)
                defaultView.addSubview(defaultButton)
                
                let selectphotofromlibraryView = UIView(frame: CGRect(x: mid, y: 161, width: wi, height: 57))
                
                selectphotofromlibraryView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
                mainViewFakeMessageWallpaper.addSubview(selectphotofromlibraryView)
                selectphotofromlibraryView.roundCorners([.bottomLeft,.bottomRight], radius: 13)
                
                let selectphotofromlibraryButton = UIButton(frame: selectphotofromlibraryView.bounds)
                selectphotofromlibraryButton.setTitle("Change Wallpaper from library ", for: .normal)
                selectphotofromlibraryButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPH(w: 22))
                selectphotofromlibraryButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                selectphotofromlibraryButton.backgroundColor = UIColor.clear
                selectphotofromlibraryButton.addTarget(self, action: #selector(SegMesChatCallController.selectphotofromlibraryActionUp(_:)), for: .touchUpInside)
                selectphotofromlibraryButton.addTarget(self, action: #selector(SegMesChatCallController.selectphotofromlibraryActionIn(_:)), for: .touchDown)
                selectphotofromlibraryButton.addTarget(self, action: #selector(SegMesChatCallController.selectphotofromlibraryActionExit(_:)), for: .touchUpOutside)
                
                let blurEffectViewselectphotofromlibraryView = UIVisualEffectView(effect: blurEffect)
                blurEffectViewselectphotofromlibraryView.frame = defaultView.bounds
                
                blurEffectViewselectphotofromlibraryView.layer.masksToBounds = false
                blurEffectViewselectphotofromlibraryView.alpha = 1
                selectphotofromlibraryView.addSubview(blurEffectViewselectphotofromlibraryView)
                selectphotofromlibraryView.addSubview(selectphotofromlibraryButton)
                
                let cancelView = UIView(frame: CGRect(x: mid, y: 225, width: wi, height: 57))
                
                cancelView.backgroundColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                mainViewFakeMessageWallpaper.addSubview(cancelView)
                cancelView.layer.cornerRadius = 13
                
                
                
                let cancelButton = UIButton(frame: selectphotofromlibraryView.bounds)
                cancelButton.setTitle("Cancel", for: .normal)
                cancelButton.titleLabel?.font =  UIFont(name: "SFProText-Semibold", size: WIPH(w: 22))
                cancelButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                cancelButton.addTarget(self, action: #selector(SegMesChatCallController.cancelActionUp(_:)), for: .touchUpInside)
                cancelButton.addTarget(self, action: #selector(SegMesChatCallController.cancelActionIn(_:)), for: .touchDown)
                cancelButton.addTarget(self, action: #selector(SegMesChatCallController.cancelActionExit(_:)), for: .touchUpOutside)
                cancelView.addSubview(cancelButton)
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    
                    self.blackViewFakeMessageWallpaper.alpha = 1
                    self.mainViewFakeMessageWallpaper.alpha = 1
                    
                    self.mainViewFakeMessageWallpaper.frame = CGRect(x: 0, y: y, width: self.mainViewFakeMessageWallpaper.frame.width, height: height)
                    
                    
                }, completion: nil)
            }
            
            
        }
    }
    
    
    @objc func showSettingCellFakeMessage(){
        if let window = UIApplication.shared.keyWindow {
            if UIScreen.main.bounds.width >= 768{
                blackViewFakeMessageCell.backgroundColor = UIColor(displayP3Red: 216/255, green: 216/255, blue: 216/255, alpha: 0.7)
                
                blackViewFakeMessageCell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismissCell)))
                
                window.addSubview(blackViewFakeMessageCell)
                window.addSubview(mainViewFakeMessageCell)
                blackViewFakeMessageCell.frame = window.frame
                blackViewFakeMessageCell.alpha = 0
                mainViewFakeMessageCell.backgroundColor = UIColor.clear
                
                let height: CGFloat = 242
                let y = window.frame.height - height
                mainViewFakeMessageCell.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
                let wi = mainViewFakeMessageCell.frame.width - 16
                
                
                let mid = (mainViewFakeMessageCell.frame.width - wi)/2
                
                let blurEffect = UIBlurEffect(style: .extraLight)
                
                let editView = UIView(frame: CGRect(x: mid, y: 0, width: wi, height: 56))
                
                editView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageCell.addSubview(editView)
                editView.clipsToBounds = true
                editView.roundCorners([.topLeft,.topRight], radius: 13)
                
                let blurEffecteditView = UIVisualEffectView(effect: blurEffect)
                blurEffecteditView.frame = editView.bounds
                
                blurEffecteditView.layer.masksToBounds = false
                blurEffecteditView.alpha = 1
                editView.addSubview(blurEffecteditView)
                let editButton = UIButton(frame: editView.bounds)
                editButton.setTitle("Edit", for: .normal)
                editButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPH(w: 22))
                editButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                editButton.addTarget(self, action: #selector(SegMesChatCallController.editActionUp(_:)), for: .touchUpInside)
                editButton.addTarget(self, action: #selector(SegMesChatCallController.editActionIn(_:)), for: .touchDown)
                editButton.addTarget(self, action: #selector(SegMesChatCallController.editActionExit(_:)), for: .touchUpOutside)
                editView.addSubview(editButton)
                
                let activeView = UIView(frame: CGRect(x: mid, y: 57, width: wi, height: 56))
                
                activeView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageCell.addSubview(activeView)
                activeView.clipsToBounds = true
                let blurEffectactiveView = UIVisualEffectView(effect: blurEffect)
                blurEffectactiveView.frame = activeView.bounds
                
                blurEffectactiveView.layer.masksToBounds = false
                blurEffectactiveView.alpha = 1
                activeView.addSubview(blurEffectactiveView)
                
                let activeButton = UIButton(frame: activeView.bounds)
                activeButton.setTitle("Active", for: .normal)
                activeButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPH(w: 22))
                activeButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                activeButton.addTarget(self, action: #selector(SegMesChatCallController.activeActionUp(_:)), for: .touchUpInside)
                activeButton.addTarget(self, action: #selector(SegMesChatCallController.activeActionIn(_:)), for: .touchDown)
                activeButton.addTarget(self, action: #selector(SegMesChatCallController.activeActionExit(_:)), for: .touchUpOutside)
                activeView.addSubview(activeButton)
                
                let deleteView = UIView(frame: CGRect(x: mid, y: 114, width: wi, height: 56))
                
                deleteView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageCell.addSubview(deleteView)
                deleteView.clipsToBounds = true
                deleteView.roundCorners([.bottomLeft,.bottomRight], radius: 13)
                let blurEffectdeleteView = UIVisualEffectView(effect: blurEffect)
                blurEffectdeleteView.frame = deleteView.bounds
                
                blurEffectdeleteView.layer.masksToBounds = false
                blurEffectdeleteView.alpha = 1
                deleteView.addSubview(blurEffectdeleteView)
                let deleteButton = UIButton(frame: activeView.bounds)
                deleteButton.setTitle("Delete", for: .normal)
                deleteButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPH(w: 22))
                deleteButton.setTitleColor(UIColor(displayP3Red: 255/255, green: 59/255, blue: 48/255, alpha: 1), for: .normal)
                
                deleteButton.addTarget(self, action: #selector(SegMesChatCallController.deleteActionUp(_:)), for: .touchUpInside)
                deleteButton.addTarget(self, action: #selector(SegMesChatCallController.deleteActionIn(_:)), for: .touchDown)
                deleteButton.addTarget(self, action: #selector(SegMesChatCallController.deleteActionExit(_:)), for: .touchUpOutside)
                deleteView.addSubview(deleteButton)
                
                let cancelView = UIView(frame: CGRect(x: mid, y: 178, width: wi, height: 56))
                
                cancelView.backgroundColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                mainViewFakeMessageCell.addSubview(cancelView)
                cancelView.clipsToBounds = true
                cancelView.layer.cornerRadius = 13
                let blurEffectcancel = UIBlurEffect(style: .light)
                let blurEffectcancelView = UIVisualEffectView(effect: blurEffectcancel)
                blurEffectcancelView.frame = cancelView.bounds
                
                blurEffectcancelView.layer.masksToBounds = false
                blurEffectcancelView.alpha = 1
                cancelView.addSubview(blurEffectcancelView)
                
                let cancelButton = UIButton(frame: cancelView.bounds)
                cancelButton.setTitle("Cancel", for: .normal)
                cancelButton.titleLabel?.font =  UIFont(name: "SFProText-Semibold", size: WIPH(w: 22))
                cancelButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                cancelButton.addTarget(self, action: #selector(SegMesChatCallController.cancelCellActionUp(_:)), for: .touchUpInside)
                cancelButton.addTarget(self, action: #selector(SegMesChatCallController.cancelActionIn(_:)), for: .touchDown)
                cancelButton.addTarget(self, action: #selector(SegMesChatCallController.cancelActionExit(_:)), for: .touchUpOutside)
                cancelView.addSubview(cancelButton)
                
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    
                    self.blackViewFakeMessageCell.alpha = 1
                    self.mainViewFakeMessageCell.alpha = 1
                    
                    
                    self.mainViewFakeMessageCell.frame = CGRect(x: 0, y: y, width: self.mainViewFakeMessageCell.frame.width, height: height)
                    
                    
                }, completion: nil)
            }else{
                blackViewFakeMessageCell.backgroundColor = UIColor(displayP3Red: 216/255, green: 216/255, blue: 216/255, alpha: 0.7)
                
                blackViewFakeMessageCell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismissCell)))
                
                window.addSubview(blackViewFakeMessageCell)
                window.addSubview(mainViewFakeMessageCell)
                blackViewFakeMessageCell.frame = window.frame
                blackViewFakeMessageCell.alpha = 0
                mainViewFakeMessageCell.backgroundColor = UIColor.clear
                
                let height: CGFloat = 242
                let y = window.frame.height - height
                mainViewFakeMessageCell.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
                let wi = mainViewFakeMessageCell.frame.width - 16
                
                
                let mid = (mainViewFakeMessageCell.frame.width - wi)/2
                
                let blurEffect = UIBlurEffect(style: .extraLight)
                
                let editView = UIView(frame: CGRect(x: mid, y: 0, width: wi, height: 56))
                
                editView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageCell.addSubview(editView)
                editView.clipsToBounds = true
                editView.roundCorners([.topLeft,.topRight], radius: 13)
                
                let blurEffecteditView = UIVisualEffectView(effect: blurEffect)
                blurEffecteditView.frame = editView.bounds
                
                blurEffecteditView.layer.masksToBounds = false
                blurEffecteditView.alpha = 1
                editView.addSubview(blurEffecteditView)
                let editButton = UIButton(frame: editView.bounds)
                editButton.setTitle("Edit", for: .normal)
                editButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPH(w: 22))
                editButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                editButton.addTarget(self, action: #selector(SegMesChatCallController.editActionUp(_:)), for: .touchUpInside)
                editButton.addTarget(self, action: #selector(SegMesChatCallController.editActionIn(_:)), for: .touchDown)
                editButton.addTarget(self, action: #selector(SegMesChatCallController.editActionExit(_:)), for: .touchUpOutside)
                editView.addSubview(editButton)
                
                let activeView = UIView(frame: CGRect(x: mid, y: 57, width: wi, height: 56))
                
                activeView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageCell.addSubview(activeView)
                activeView.clipsToBounds = true
                let blurEffectactiveView = UIVisualEffectView(effect: blurEffect)
                blurEffectactiveView.frame = activeView.bounds
                
                blurEffectactiveView.layer.masksToBounds = false
                blurEffectactiveView.alpha = 1
                activeView.addSubview(blurEffectactiveView)
                
                let activeButton = UIButton(frame: activeView.bounds)
                activeButton.setTitle("Active", for: .normal)
                activeButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPH(w: 22))
                activeButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                activeButton.addTarget(self, action: #selector(SegMesChatCallController.activeActionUp(_:)), for: .touchUpInside)
                activeButton.addTarget(self, action: #selector(SegMesChatCallController.activeActionIn(_:)), for: .touchDown)
                activeButton.addTarget(self, action: #selector(SegMesChatCallController.activeActionExit(_:)), for: .touchUpOutside)
                activeView.addSubview(activeButton)
                
                let deleteView = UIView(frame: CGRect(x: mid, y: 114, width: wi, height: 56))
                
                deleteView.backgroundColor = UIColor(displayP3Red: 248/255, green: 248/255, blue: 248/255, alpha: 0.82)
                mainViewFakeMessageCell.addSubview(deleteView)
                deleteView.clipsToBounds = true
                deleteView.roundCorners([.bottomLeft,.bottomRight], radius: 13)
                let blurEffectdeleteView = UIVisualEffectView(effect: blurEffect)
                blurEffectdeleteView.frame = deleteView.bounds
                
                blurEffectdeleteView.layer.masksToBounds = false
                blurEffectdeleteView.alpha = 1
                deleteView.addSubview(blurEffectdeleteView)
                let deleteButton = UIButton(frame: activeView.bounds)
                deleteButton.setTitle("Delete", for: .normal)
                deleteButton.titleLabel?.font =  UIFont(name: "SFProText-Regular", size: WIPH(w: 22))
                deleteButton.setTitleColor(UIColor(displayP3Red: 255/255, green: 59/255, blue: 48/255, alpha: 1), for: .normal)
                
                deleteButton.addTarget(self, action: #selector(SegMesChatCallController.deleteActionUp(_:)), for: .touchUpInside)
                deleteButton.addTarget(self, action: #selector(SegMesChatCallController.deleteActionIn(_:)), for: .touchDown)
                deleteButton.addTarget(self, action: #selector(SegMesChatCallController.deleteActionExit(_:)), for: .touchUpOutside)
                deleteView.addSubview(deleteButton)
                
                let cancelView = UIView(frame: CGRect(x: mid, y: 178, width: wi, height: 56))
                
                cancelView.backgroundColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                mainViewFakeMessageCell.addSubview(cancelView)
                cancelView.clipsToBounds = true
                cancelView.layer.cornerRadius = 13
                let blurEffectcancel = UIBlurEffect(style: .light)
                let blurEffectcancelView = UIVisualEffectView(effect: blurEffectcancel)
                blurEffectcancelView.frame = cancelView.bounds
                
                blurEffectcancelView.layer.masksToBounds = false
                blurEffectcancelView.alpha = 1
                cancelView.addSubview(blurEffectcancelView)
                
                let cancelButton = UIButton(frame: cancelView.bounds)
                cancelButton.setTitle("Cancel", for: .normal)
                cancelButton.titleLabel?.font =  UIFont(name: "SFProText-Semibold", size: WIPH(w: 22))
                cancelButton.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                
                cancelButton.addTarget(self, action: #selector(SegMesChatCallController.cancelCellActionUp(_:)), for: .touchUpInside)
                cancelButton.addTarget(self, action: #selector(SegMesChatCallController.cancelActionIn(_:)), for: .touchDown)
                cancelButton.addTarget(self, action: #selector(SegMesChatCallController.cancelActionExit(_:)), for: .touchUpOutside)
                cancelView.addSubview(cancelButton)
                
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    
                    self.blackViewFakeMessageCell.alpha = 1
                    self.mainViewFakeMessageCell.alpha = 1
                    
                    
                    self.mainViewFakeMessageCell.frame = CGRect(x: 0, y: y, width: self.mainViewFakeMessageCell.frame.width, height: height)
                    
                    
                }, completion: nil)
            }
        }
    }
    
    @objc func deleteActionUp(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 255/255, green: 59/255, blue: 48/255, alpha: 1), for: .normal)
        FakeMessageHandleCoreData.share.delete(id: (FakeMessage_Edit?.id)!)
        handleDismissCell()
        NotificationCenter.default.post(name: .reloadSeg, object: nil)
        
        
    }
    @objc func deleteActionIn(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 255/255, green: 59/255, blue: 48/255, alpha: 0.2), for: .normal)
    }
    @objc func deleteActionExit(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 255/255, green: 59/255, blue: 48/255, alpha: 1), for: .normal)
    }
    
    
    
    @objc func activeActionUp(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        
        
        let alert1 = UIAlertController(title: "Warring", message: "You just have actived message. Now! This app changing screen active! You need to wait into time your set The Message is showwing!", preferredStyle: UIAlertControllerStyle.alert)
        alert1.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ action in
            let alert2 = UIAlertController(title: "Warring", message: "You want to change Active Mode or Sleep Mode on Active Screen", preferredStyle: UIAlertControllerStyle.alert)
            self.present(alert2, animated: true, completion: nil)
            alert2.addAction(UIAlertAction(title: "Active Mode", style: UIAlertActionStyle.default, handler: { action in
                
                let alert3 = UIAlertController(title: "Warring", message: "When app in Active Screen You can go back by Tap On Screen! Or You can wait the message is coming! Enjoy!!!!", preferredStyle: UIAlertControllerStyle.alert)
                self.present(alert3, animated: true, completion: nil)
                FakeMessage_ActiveMode = 0
                alert3.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                    
                    let pickImageVC = self.storyboard?.instantiateViewController(withIdentifier: "ActiveViewController") as! ActiveViewController
                    pickImageVC.modalPresentationStyle = .fullScreen
                    self.present(pickImageVC, animated: false, completion: nil)
                    
                }))
            }))
            
            alert2.addAction(UIAlertAction(title: "Sleep Mode", style: UIAlertActionStyle.default, handler: { action in
                let alert3 = UIAlertController(title: "Warring", message: "When app in Active Screen You can go back by Tap On Screen! Or You can wait the message is coming! Enjoy!!!! In Sleep mode you screen is DARK don't worry! You just have waited the message to coming! Enjoy!!!!", preferredStyle: UIAlertControllerStyle.alert)
                self.present(alert3, animated: true, completion: nil)
                FakeMessage_ActiveMode = 1
                alert3.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                    
                    let pickImageVC = self.storyboard?.instantiateViewController(withIdentifier: "ActiveViewController") as! ActiveViewController
                    pickImageVC.modalPresentationStyle = .fullScreen
                    self.present(pickImageVC, animated: false, completion: nil)
                    
                }))
                
                
            }))
        
    }))
        self.present(alert1, animated: true, completion: nil)
        
        
        handleDismissCell()
        
        
    }
    @objc func activeActionIn(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 0.2), for: .normal)
    }
    @objc func activeActionExit(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
    }
    
    
    
    @objc func editActionUp(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        FakeMessage_typeCreateEdit = 1
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateAndEdit") as? CreateAndEditController {
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
        handleDismissCell()
        
        
    }
    @objc func editActionIn(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 0.2), for: .normal)
    }
    @objc func editActionExit(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
    }
    
    
    
    
    @objc func previewActionUp(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        let pickImageVC = self.storyboard?.instantiateViewController(withIdentifier: "PreviewController") as! PreviewController
        pickImageVC.modalPresentationStyle = .fullScreen
        self.present(pickImageVC, animated: true, completion: nil)
        self.handleDismiss()
        
    }
    @objc func previewActionIn(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 0.2), for: .normal)
    }
    @objc func previewActionExit(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
    }
    
    
    @objc func defaultActionUp(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        UserDefaults.standard.set(0, forKey: "WallpaperChoose")
        self.handleDismiss()
        showAlert(title: "Warring", message: "You have changed the Wallpaper Default in the preview of the application. Please select Preview a message to show it!")
    }
    @objc func defaultActionIn(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 0.2), for: .normal)
    }
    @objc func defaultActionExit(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
    }
    @objc func selectphotofromlibraryActionUp(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        self.handleDismiss()
    }
    @objc func selectphotofromlibraryActionIn(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 0.2), for: .normal)
    }
    @objc func selectphotofromlibraryActionExit(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
    }
    @objc func cancelActionUp(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        self.handleDismiss()
    }
    @objc func cancelCellActionUp(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        self.handleDismissCell()
    }
    @objc func cancelActionIn(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 0.2), for: .normal)
    }
    @objc func cancelActionExit(_ sender:UIButton!)
    {
        
        sender.setTitleColor(UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
    }
    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5) {
            self.blackViewFakeMessageWallpaper.alpha = 0
            self.mainViewFakeMessageWallpaper.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.mainViewFakeMessageWallpaper.frame = CGRect(x: 0, y: window.frame.height, width: self.mainViewFakeMessageWallpaper.frame.width, height: self.mainViewFakeMessageWallpaper.frame.height)
            }
        }
    }
    @objc func handleDismissCell() {
        UIView.animate(withDuration: 0.5) {
            self.blackViewFakeMessageCell.alpha = 0
            self.mainViewFakeMessageCell.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.mainViewFakeMessageCell.frame = CGRect(x: 0, y: window.frame.height, width: self.mainViewFakeMessageCell.frame.width, height: self.mainViewFakeMessageCell.frame.height)
            }
        }
    }

    @objc func showSetWallpapercompelet(){
        let alert = UIAlertController(title: "Warring", message: "You have changed the New Wallpaper in the preview of the application. Please select Preview a message to show it!", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            
            
            
        }))
    }
    // Func Of Fake Message
    
// BannerView Setup Func
    private func offBanner(){
        heightOfBannerView.constant = -50
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Trasition View Controller
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
            vc.view.frame = self.Fake.bounds
            self.Fake.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.FakeMessage.rawValue :
            vc = FakeMessageChildTabVC
        case TabIndex.FakeChat.rawValue :
            vc = FakeChatChildTabVC
        case TabIndex.FakeCall.rawValue :
            vc = FakeCallChildTabVC
        default:
            return nil
        }
        
        return vc
    }
    
    // Action Seg
    
    @IBAction func SegChange(_ sender: UISegmentedControl) {
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    
    
}



// PickerImageController

extension SegMesChatCallController:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageDataTemp = pickedImage.fixOrientation()
            
        }
        
        
        let pickImageVC = self.storyboard?.instantiateViewController(withIdentifier: "PickImage") as! PickImageController
        self.imagePicker.present(pickImageVC, animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
    
    @objc func Diss(){
        dismiss(animated: true, completion:nil)
    }
}

