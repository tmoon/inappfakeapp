//
//  ChooseChatController.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 03/05/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import UIKit

class ChooseChatController: UIViewController {

    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBegan()
        // Do any additional setup after loading the view.
    }

    func setupBegan(){
        tableView.delegate = self
        tableView.dataSource = self
        var si:CGFloat = 0
        if UIScreen.main.bounds.width >= 768{
            si = WIPA(w: 20)
        }else{
            si = WIPH(w: 20)
        }
        
        let attr = [
            
            NSAttributedStringKey.font:UIFont(name:"SFProText-Semibold", size: si)!,
            NSAttributedStringKey.foregroundColor: UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0)
        ]
        let title = NSAttributedString(string: "Choose Icon", attributes: attr)
        btnBack.set(image: #imageLiteral(resourceName: "iconBack"), attributedTitle: title, at: .right, width: 14.0, state: .normal)
//        btnBack.set(image: #imageLiteral(resourceName: "iconBack"), title: title, titlePosition: .right, additionalSpacing: 8.0, state: .normal)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func abtnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


// TableView

extension ChooseChatController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIScreen.main.bounds.width >= 768{
            return WIPA(w: 57)
        }
        return WIPH(w: 57)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ChooseChatCell
        cell.imageChat.image = UIImage(named: dataIcon[indexPath.row])
        cell.nameChat.text = dataTypeShow[indexPath.row]
    
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        FakeMessage_ChooseType = indexPath.row
       navigationController?.popViewController(animated: true)
        
    }
}
