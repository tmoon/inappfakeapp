//
//  FakeMessageController.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 18/04/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import UIKit
import ConnectFramework

class FakeMessageController: UIViewController{

    
    
    
    
    // Core Data
    var fakemessageData:[FakeMessageEntity] = []
    // Core Data
   
    
    // Collec Var
    @IBOutlet weak var collectionFakeMessage: UICollectionView!
    // Collec Var
    
    // library var
    @IBOutlet weak var traillingBtnLibrary: NSLayoutConstraint!
    
    @IBOutlet weak var centerBtnLibrary: NSLayoutConstraint!
    // library var
    
    // set up view var
    @IBOutlet weak var widthHead: NSLayoutConstraint!
    
    @IBOutlet weak var btnAdd: UIButton!
    
    
    // set up view var
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // setup view func call
        self.setupViewFakeMessage()
        self.registerNotification()
        
        
        // Do any additional setup after loading the view.
    }
    // registerNotification
    func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadSeg), name: .reloadSeg , object: nil)
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidAppear(_ animated: Bool) {
        // Fill Data
        self.fillDataFakemessage()
        
        
    }
    // setup fill data
    private func fillDataFakemessage(){
        
        self.fakemessageData = FakeMessageHandleCoreData.share.getAllData()
        collectionFakeMessage.reloadData()
    }
    // setup view func
    private func setupViewFakeMessage(){
        if UIScreen.main.bounds.width >= 768{
            self.widthHead.constant = WIPA(w: 556)
            self.traillingBtnLibrary.priority = .defaultLow
            self.centerBtnLibrary.priority = .defaultHigh
        }else{
            self.widthHead.constant = WIPH(w: self.widthHead.constant)
            self.traillingBtnLibrary.priority = .defaultHigh
            self.centerBtnLibrary.priority = .defaultLow
        }
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: self.widthHead.constant,height: btnAdd.frame.size.height))
        btnAdd.layer.shadowColor = UIColor.black.cgColor
        btnAdd.layer.cornerRadius = 13
        btnAdd.layer.shadowOffset = CGSize(width: 0, height: 5)  //Here you control x and y
        btnAdd.layer.shadowOpacity = 0.25
        btnAdd.layer.shadowRadius = 10 //Here your control your blur
        btnAdd.layer.masksToBounds =  false
        btnAdd.layer.shadowPath = shadowPath.cgPath
        
        let MaskBlur = UIView(frame: btnAdd.bounds)

        MaskBlur.layer.cornerRadius = 13
        MaskBlur.clipsToBounds = true
        MaskBlur.backgroundColor = UIColor.clear

        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = MaskBlur.bounds
        blurEffectView.layer.cornerRadius = 13
        blurEffectView.layer.masksToBounds = false
        blurEffectView.alpha = 0.2
        MaskBlur.addSubview(blurEffectView)
        
//        let blur = UIVisualEffectView(effect: UIBlurEffect(style:
//            UIBlurEffectStyle.light))
//        blur.frame = btnAdd.bounds
        MaskBlur.isUserInteractionEnabled = false //This allows touches to forward to the button.
        btnAdd.insertSubview(MaskBlur, at: 0)
        btnAdd.bringSubview(toFront: btnAdd.imageView!)
        
    }
    
    private func changeTime(time:String)->String{
        if time == "0"{
            return "now"
        }
        return time + "m ago"
    }
    @objc func reloadSeg(){
        self.fakemessageData = FakeMessageHandleCoreData.share.getAllData()
        collectionFakeMessage.reloadData()
    }
    
    // Action button
        // Library
    
    @IBAction func abtnLibrary(_ sender: Any) {
        if ConnectHelper.checkUserPurchased() == true {
            showSettingLibrary()
        } else {
            ConnectHelper.showPurchaseController(inViewController: self) { [weak self] (isPurchase) in
                if isPurchase == true {
                    self?.showSettingLibrary()
                }
            }
        }
    }
    
    func showSettingLibrary() {
        NotificationCenter.default.post(name: .showSettingsLibraryOfFakeMessage, object: nil)
    }
    
    // Create
    @IBAction func abtnCreate(_ sender: Any) {
        if ConnectHelper.checkUserPurchased() == true {
            showCreate()
        } else {
            ConnectHelper.showPurchaseController(inViewController: self) { [weak self] (isPurchase) in
                if isPurchase == true {
                    self?.showCreate()
                }
            }
        }
    }
    
    func showCreate() {
        FakeMessage_typeCreateEdit = 0
        FakeMessage_ChooseType = 0
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateAndEdit") as? CreateAndEditController {

            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FakeMessageController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fakemessageData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellFakeMessage", for: indexPath) as! FakeMessageCell
        
        
        // Design UI cell
        
        cell.backgroundColor = UIColor.white
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: cell.frame.size.width,height: cell.frame.size.height))
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.cornerRadius = 13
        cell.layer.shadowOffset = CGSize(width: 0, height: 5)  //Here you control x and y
        cell.layer.shadowOpacity = 0.25
        cell.layer.shadowRadius = 10 //Here your control your blur
        cell.layer.masksToBounds =  false
        cell.layer.shadowPath = shadowPath.cgPath

        
        
        
        
        
        // Fill Data
        let typeChat = Int(fakemessageData[indexPath.row].icon!)
        let name = fakemessageData[indexPath.row].name
        let message = fakemessageData[indexPath.row].message
        let time = fakemessageData[indexPath.row].time
        cell.bringSubview(toFront: cell.iconChat)
        cell.bringSubview(toFront: cell.typeChat)
        cell.bringSubview(toFront: cell.name)
        cell.bringSubview(toFront: cell.message)
        cell.bringSubview(toFront: cell.timeCell)
        cell.timeCell.text = changeTime(time: time!)
        cell.iconChat.image = UIImage(named: dataIcon[typeChat!])
        cell.typeChat.text = dataType[typeChat!]
        cell.name.text = name!
        cell.message.text = message!
        cell.message.numberOfLines = 4
        cell.message.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.message.frame = CGRect(x: cell.message.frame.origin.x, y: cell.message.frame.origin.y, width: cell.message.frame.size.width, height: cell.message.optimalHeight)
        
        // Blur Cell
        let messageText = fakemessageData[indexPath.row].message
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        if let mess = messageText{
             if UIScreen.main.bounds.width >= 768{
                
                if UIScreen.main.bounds.width == 1024{
                    let size = CGSize(width: WIPA(w: 556), height: 80)
                    let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                    let si = CGSize(width: WIPA(w: 556), height: estimatedFrame.height + 85)
                    cell.MaskBlur.layer.cornerRadius = 13
                    cell.MaskBlur.clipsToBounds = true
                    cell.MaskBlur.backgroundColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.54)
                    
                    let blurEffect = UIBlurEffect(style: .extraLight)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = CGRect(x: 0, y: 0, width: si.width, height: si.height)
                    blurEffectView.layer.cornerRadius = 13
                    blurEffectView.layer.masksToBounds = false
                    blurEffectView.alpha = 1
                    cell.MaskBlur.addSubview(blurEffectView)
                }else{
                    let size = CGSize(width: WIPA(w: 556), height: 80)
                    let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                    let si = CGSize(width: WIPA(w: 556), height: estimatedFrame.height + 67)
                    cell.MaskBlur.layer.cornerRadius = 13
                    cell.MaskBlur.clipsToBounds = true
                    cell.MaskBlur.backgroundColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.54)
                    
                    let blurEffect = UIBlurEffect(style: .extraLight)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = CGRect(x: 0, y: 0, width: si.width, height: si.height)
                    blurEffectView.layer.cornerRadius = 13
                    blurEffectView.layer.masksToBounds = false
                    blurEffectView.alpha = 1
                    cell.MaskBlur.addSubview(blurEffectView)
                }
             }else{
                let size = CGSize(width: WIPH(w: 359), height: 80)
                let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                let si = CGSize(width: WIPH(w: 359), height: estimatedFrame.height + 67)
                cell.MaskBlur.layer.cornerRadius = 13
                cell.MaskBlur.clipsToBounds = true
                cell.MaskBlur.backgroundColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.54)
                
                let blurEffect = UIBlurEffect(style: .extraLight)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame = CGRect(x: 0, y: 0, width: si.width, height: si.height)
                blurEffectView.layer.cornerRadius = 13
                blurEffectView.layer.masksToBounds = false
                blurEffectView.alpha = 1
                cell.MaskBlur.addSubview(blurEffectView)
            }
            
        }
        
        
        
        
        
        
        

        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let messageText = fakemessageData[indexPath.row].message
        if UIScreen.main.bounds.width >= 768{
            if UIScreen.main.bounds.width == 1024{
                let size = CGSize(width: WIPA(w: 556), height: 80)
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                if let mess = messageText{
                    let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                    return CGSize(width: WIPA(w: 556), height: estimatedFrame.height + 85)
                }
            }else{
                let size = CGSize(width: WIPA(w: 556), height: 80)
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                if let mess = messageText{
                    let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                    return CGSize(width: WIPA(w: 556), height: estimatedFrame.height + 67)
                }
            }
            
        }else{
            let size = CGSize(width: WIPH(w: 359), height: 80)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            if let mess = messageText{
                let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                return CGSize(width: WIPH(w: 359), height: estimatedFrame.height + 67)
            }
        }
        
        return CGSize(width: WIPH(w: 359), height: 87)
    }
    // khoảng cách của thằng top collection chính với cái phụ
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if ConnectHelper.checkUserPurchased() == true {
            tapToCell(indexPath: indexPath)
        } else {
            ConnectHelper.showPurchaseController(inViewController: self) { [weak self] (isPurchase) in
                if isPurchase == true {
                    self?.tapToCell(indexPath: indexPath)
                }
            }
        }
    }
    
    func tapToCell(indexPath: IndexPath) {
        openSettingCell()
        let typeChat = Int(fakemessageData[indexPath.row].icon!)
        FakeMessage_ChooseType = typeChat!
        FakeMessage_Edit = fakemessageData[indexPath.row]
    }
    
    func openSettingCell(){
        NotificationCenter.default.post(name: .showSettingCellFakeMessage, object: nil)
    }
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        var visibleRect = CGRect()
//        
//        visibleRect.origin = collectionFakeMessage.contentOffset
//        visibleRect.size = collectionFakeMessage.bounds.size
//        
//        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
//        
//        guard let indexPath = collectionFakeMessage.indexPathForItem(at: visiblePoint) else { return }
//        
//        indexScrollAuto = indexPath
//    }
}



extension UILabel
{
    var optimalHeight : CGFloat
    {
        get
        {
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = self.lineBreakMode
            label.font = self.font
            label.text = self.text
            
            label.sizeToFit()
            
            return label.frame.height
        }
    }
}
