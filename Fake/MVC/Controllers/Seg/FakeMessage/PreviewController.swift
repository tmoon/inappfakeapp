//
//  PreviewController.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 26/04/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import UIKit
import Telefon
import Social
class PreviewController: UIViewController, UIDocumentInteractionControllerDelegate {
    
    var documentController: UIDocumentInteractionController!
    
    let sim = SIM()
    
    @IBOutlet weak var CarrierName: UILabel!
    @IBOutlet weak var screenshot: UIView!
    
    @IBOutlet weak var T: DesignableLabel!
    
    @IBOutlet weak var D: DesignableLabel!
    
    var updata:Timer?
    
    
    @IBOutlet weak var collectionFakeMessage: UICollectionView!
    
    // Core Data
    var fakemessageData:[FakeMessageEntity] = []
    // Core Data
    @IBOutlet weak var WallpaperImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(true, forKey: "FakeMessage_Interstitial_Preview")
        screenshot.frame = self.view.bounds
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: screenshot.frame.size.width,height: screenshot.frame.size.height))
        screenshot.layer.shadowColor = UIColor.black.cgColor
        
        screenshot.layer.shadowOffset = CGSize(width: 0, height: 0)  //Here you control x and y
        screenshot.layer.shadowOpacity = 1
        screenshot.layer.shadowRadius = 10 //Here your control your blur
        screenshot.layer.masksToBounds =  false
        screenshot.layer.shadowPath = shadowPath.cgPath
        
        self.setupStatusBar()
        self.setupTimer()
       
        self.updata = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { _ in
            self.setupTimer()
        })
        self.setWallpaper()
        // Fill Data
        self.fillDataFakemessage()
        
        
        screenshot.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
        
        // Do any additional setup after loading the view.
    }

    func setupStatusBar(){
       
        if sim.allowsVOIP.description != "false"{
            self.CarrierName.text = sim.carrierName
        }
        
    }
    open func takeScreenshot() -> UIImage? {
       
        
        UIGraphicsBeginImageContext(view.frame.size)
        screenshot.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func setupTimer(){
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        let day = calendar.component(.day, from: date)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE"
        let weekday = dateFormatterPrint.string(from: date)
        let month = date.getMonthName()
        
        
       
        if minutes < 10{
            T.text = "\(hour):0\(minutes)"
        }else{
            T.text = "\(hour):\(minutes)"
        }
        
        D.text = "\(weekday), \(day) \(month)"
        
    }
    func setWallpaper(){
        
        if UserDefaults.standard.integer(forKey: "WallpaperChoose") == 0{
            if UIScreen.main.bounds.width >= 768{
                WallpaperImage.image =  #imageLiteral(resourceName: "wallpaper_Ipad")
            }else{
                WallpaperImage.image =  #imageLiteral(resourceName: "wallpaper_Iphone")
            }
        }else{
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let filePath = documentsURL.appendingPathComponent("Wallpaper.png").path
            WallpaperImage.image = UIImage(contentsOfFile: filePath)
        }
        
    }
    private func changeTime(time:String)->String{
        if time == "0"{
            return "now"
        }
        return time + "m ago"
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    // setup fill data
    private func fillDataFakemessage(){
        
        self.fakemessageData = FakeMessageHandleCoreData.share.getAllData()
        collectionFakeMessage.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func abtnDownload(_ sender: Any) {
        
        UIImageWriteToSavedPhotosAlbum(takeScreenshot()!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
    }
    
    @IBAction func abtnShare(_ sender: Any) {
        let textToShare = takeScreenshot()
        
       
        let objectsToShare = [textToShare] as! [UIImage]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = sender as? UIView
        self.present(activityVC, animated: true, completion: nil)
        
    }
    
    
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "You have successfully downloaded the image to your photo library! Now open photo library to check!!!!", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    @IBAction func abtnShareFacebook(_ sender: Any) {
        let textToShare = takeScreenshot()
        
        
        let objectsToShare = [textToShare] as! [UIImage]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.excludedActivityTypes = [.postToTwitter,.postToWeibo,.message,.mail,.print,.copyToPasteboard,.assignToContact,.saveToCameraRoll,.saveToCameraRoll,.addToReadingList,.postToFlickr,.postToVimeo,.postToTencentWeibo,.airDrop,.openInIBooks]
        
        activityVC.popoverPresentationController?.sourceView = sender as? UIView
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func abtnShareImta(_ sender: Any) {
        DispatchQueue.main.async {
            
            //Share To Instagram:
            let instagramURL = URL(string: "instagram://app")
            if UIApplication.shared.canOpenURL(instagramURL!) {
                
                let imageData = UIImageJPEGRepresentation(self.takeScreenshot()!, 100)
                let writePath = (NSTemporaryDirectory() as NSString).appendingPathComponent("instagram.igo")
                
                do {
                    try imageData?.write(to: URL(fileURLWithPath: writePath), options: .atomic)
                } catch {
                    print(error)
                }
                
                let fileURL = URL(fileURLWithPath: writePath)
                self.documentController = UIDocumentInteractionController(url: fileURL)
                self.documentController.delegate = self
                self.documentController.uti = "com.instagram.exlusivegram"
            
                if UIDevice.current.userInterfaceIdiom == .phone {
                    self.documentController.presentOpenInMenu(from: self.view.bounds, in: self.view, animated: true)
                } else {
                    self.documentController.presentOpenInMenu(from: self.view.bounds, in: self.view, animated: true)
                }
            } else {
                print(" Instagram is not installed ")
            }
        }
    }
    
    
    
    
    
    @IBAction func abtnExit(_ sender: Any) {
        dismiss(animated: true, completion:nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PreviewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fakemessageData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellFakeMessage", for: indexPath) as! FakeMessageCell
        
        
        // Design UI cell
        
        
       
        cell.layer.cornerRadius = 13
        
        
        
        
        
        
        
        // Fill Data
        let typeChat = Int(fakemessageData[indexPath.row].icon!)
        let name = fakemessageData[indexPath.row].name
        let message = fakemessageData[indexPath.row].message
        let time = fakemessageData[indexPath.row].time
        cell.bringSubview(toFront: cell.iconChat)
        cell.bringSubview(toFront: cell.typeChat)
        cell.bringSubview(toFront: cell.name)
        cell.bringSubview(toFront: cell.message)
        cell.bringSubview(toFront: cell.timeCell)
        cell.timeCell.text = changeTime(time: time!)
        cell.iconChat.image = UIImage(named: dataIcon[typeChat!])
        cell.typeChat.text = dataType[typeChat!]
        cell.name.text = name!
        cell.message.text = message!
        cell.message.numberOfLines = 4
        cell.message.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.message.frame = CGRect(x: cell.message.frame.origin.x, y: cell.message.frame.origin.y, width: cell.message.frame.size.width, height: cell.message.optimalHeight)
        
        // Blur Cell
        let messageText = fakemessageData[indexPath.row].message
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        if let mess = messageText{
            if UIScreen.main.bounds.width >= 768{
                if UIScreen.main.bounds.width == 1024{
                    let size = CGSize(width: WIPA(w: 556), height: 80)
                    let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                    let si = CGSize(width: WIPA(w: 556), height: estimatedFrame.height + 85)
                    cell.MaskBlur.layer.cornerRadius = 13
                    cell.MaskBlur.clipsToBounds = true
                    cell.MaskBlur.backgroundColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.54)
                    
                    let blurEffect = UIBlurEffect(style: .extraLight)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = CGRect(x: 0, y: 0, width: si.width, height: si.height)
                    blurEffectView.layer.cornerRadius = 13
                    blurEffectView.layer.masksToBounds = false
                    blurEffectView.alpha = 1
                    cell.MaskBlur.addSubview(blurEffectView)
                }else{
                    let size = CGSize(width: WIPA(w: 556), height: 80)
                    let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                    let si = CGSize(width: WIPA(w: 556), height: estimatedFrame.height + 67)
                    cell.MaskBlur.layer.cornerRadius = 13
                    cell.MaskBlur.clipsToBounds = true
                    cell.MaskBlur.backgroundColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.54)
                    
                    let blurEffect = UIBlurEffect(style: .extraLight)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = CGRect(x: 0, y: 0, width: si.width, height: si.height)
                    blurEffectView.layer.cornerRadius = 13
                    blurEffectView.layer.masksToBounds = false
                    blurEffectView.alpha = 1
                    cell.MaskBlur.addSubview(blurEffectView)
                }
            }else{
                let size = CGSize(width: WIPH(w: 359), height: 80)
                let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                let si = CGSize(width: WIPH(w: 359), height: estimatedFrame.height + 67)
                cell.MaskBlur.layer.cornerRadius = 13
                cell.MaskBlur.clipsToBounds = true
                //cell.MaskBlur.backgroundColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.54)
                
                let blurEffect = UIBlurEffect(style: .extraLight)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame = CGRect(x: 0, y: 0, width: si.width, height: si.height)
                blurEffectView.layer.cornerRadius = 13
                blurEffectView.layer.masksToBounds = false
                blurEffectView.alpha = 0.9
                cell.MaskBlur.addSubview(blurEffectView)
            }
            
        }
        
        
        
        
        
        
        
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let messageText = fakemessageData[indexPath.row].message
        if UIScreen.main.bounds.width >= 768{
            if UIScreen.main.bounds.width == 1024{
                let size = CGSize(width: WIPA(w: 556), height: 80)
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                if let mess = messageText{
                    let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                    return CGSize(width: WIPA(w: 556), height: estimatedFrame.height + 85)
                }
            }else{
                let size = CGSize(width: WIPA(w: 556), height: 80)
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                if let mess = messageText{
                    let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                    return CGSize(width: WIPA(w: 556), height: estimatedFrame.height + 67)
                }
            }
        }else{
            let size = CGSize(width: WIPH(w: 359), height: 80)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            if let mess = messageText{
                let estimatedFrame = NSString(string: mess).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "SFProText-Regular", size: 15)!], context: nil)
                return CGSize(width: WIPH(w: 359), height: estimatedFrame.height + 67)
            }
        }
        
        return CGSize(width: WIPH(w: 359), height: 87)
    }
    // khoảng cách của thằng top collection chính với cái phụ
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
}
