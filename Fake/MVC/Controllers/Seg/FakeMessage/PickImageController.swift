//
//  PickImageController.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 26/04/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import UIKit

class PickImageController: UIViewController {

    
    
    
    @IBOutlet weak var T: DesignableLabel!
    
    @IBOutlet weak var D: DesignableLabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var imageView = UIImageView()
    
    var updata:Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTimer()
        self.updata = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { _ in
            self.setupTimer()
        })
        
        scrollView.delegate = self
        
        scrollView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        imageView.image = imageDataTemp
        imageView.isUserInteractionEnabled = true
        scrollView.addSubview(imageView)
        
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.frame = CGRect(x: 0, y: 0, width: imageDataTemp.size.width, height: imageDataTemp.size.height)
        
        
        scrollView.contentSize = imageDataTemp.size
        
        
        
        let scrollViewFrame = scrollView.frame
        
        
        let scaleWidth = scrollViewFrame.size.width / scrollView.contentSize.width
        let scaleHight = scrollViewFrame.size.height / scrollView.contentSize.height
        let minScale = max(scaleHight, scaleWidth)
        
        scrollView.minimumZoomScale = minScale
        scrollView.maximumZoomScale = 1
        scrollView.zoomScale = minScale
        centerScrollViewContent()
        // Do any additional setup after loading the view.
    }
    func setupTimer(){
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        let day = calendar.component(.day, from: date)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE"
        let weekday = dateFormatterPrint.string(from: date)
        let month = date.getMonthName()
        
        
        
        if minutes < 10{
            T.text = "\(hour):0\(minutes)"
        }else{
            T.text = "\(hour):\(minutes)"
        }
        
        D.text = "\(weekday), \(day) \(month)"
        
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func abtnCancel(_ sender: Any) {
        NotificationCenter.default.post(name: .Diss, object: nil)
        self.updata?.invalidate()
        self.updata = nil
    }
    
    
    @IBAction func abtnSet(_ sender: Any) {
        self.updata?.invalidate()
        self.updata = nil
        UserDefaults.standard.set(1, forKey: "WallpaperChoose")
        let ratio = imageView.image!.size.height / scrollView.contentSize.height
        let origin = CGPoint(x: scrollView.contentOffset.x * ratio, y: scrollView.contentOffset.y * ratio)
        let size = CGSize(width: scrollView.bounds.size.width * ratio,height: scrollView.bounds.size.height * ratio)
        let cropFrame = CGRect(origin: origin, size: size)
        let croppedImage = imageView.image!.croppedInRect(rect: cropFrame)
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = documentsURL.appendingPathComponent("Wallpaper.png")
        do{
            if let pngImageData = UIImagePNGRepresentation(croppedImage) {
                try pngImageData.write(to: fileURL, options: .atomic)
            }
        }catch{}
        
        showSetWallpapercompeletBool = true
        NotificationCenter.default.post(name: .Diss, object: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PickImageController: UIScrollViewDelegate{
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
        centerScrollViewContent()
        
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    func centerScrollViewContent(){
        let boundsSize = scrollView.bounds.size
        var contentsFrame = imageView.frame
        
        if contentsFrame.size.width < boundsSize.width{
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width)/2
        }else{
            contentsFrame.origin.x = 0
        }
        if contentsFrame.size.height < boundsSize.height{
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height)/2
        }else{
            contentsFrame.origin.y = 0
        }
        
        imageView.frame = contentsFrame
    }
}

