//
//  CreateAndEditController.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 27/04/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import UIKit

class CreateAndEditController: UIViewController,UITextFieldDelegate {

    var rowData = 0
    
    @IBOutlet weak var btnChooseTime: UIButton!
    @IBOutlet weak var ViewOfPickerView: UIView!
    
    var pickerData:[String] = []
    
    @IBOutlet weak var bottomPickerView: NSLayoutConstraint!
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var heightIphone: NSLayoutConstraint!
    
    @IBOutlet weak var heightIpad: NSLayoutConstraint!
    
    
    @IBOutlet weak var typeChat: UIButton!
    var move:CGRect!
    var hold:CGRect!
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var messageTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBegan()
        if self.check(){
            self.Create()
        }else{
            self.Edit()
        }
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if self.check(){
            
        }else{
            pickerView.selectRow(self.getindexPickerViewScrollEdit(value: (FakeMessage_Edit?.time)!), inComponent: 0, animated: false)
        }
    }
    func check()->Bool{
        if FakeMessage_typeCreateEdit == 0{
            return true
        }
        return false
    }
    func Create(){
        
    }
    func Edit(){
        nameTextField.text = FakeMessage_Edit?.name
        messageTextField.text = FakeMessage_Edit?.message
        typeChat.setTitle(dataTypeShow[Int((FakeMessage_Edit?.icon)!)!], for: .normal)
        btnChooseTime.setTitle(changeTime(time: (FakeMessage_Edit?.time)!), for: .normal)
        self.rowData = Int((FakeMessage_Edit?.time)!)!
        
    }
    func getindexPickerViewScrollEdit(value:String)->Int{
        
        
        
        return pickerData.index(of: changeTime(time: value))!
    }
    private func changeTime(time:String)->String{
        if time == "0"{
            return "Now"
        }
        return time + "m ago"
    }
    func setupBegan(){
        self.bottomPickerView.constant = self.pickerView.frame.size.height
        self.ViewOfPickerView.alpha = 0
        viewMain.clipsToBounds = true
        viewMain.layer.cornerRadius = 13
        self.nameTextField.delegate = self
        self.messageTextField.delegate = self
        move = CGRect(x: self.viewMain.frame.origin.x, y: 30, width: self.viewMain.frame.size.width, height: self.viewMain.frame.size.height)
        
        hold = self.viewMain.frame
        
        if UIScreen.main.bounds.width >= 768{
            heightIphone.priority = .defaultLow
            heightIphone.priority = .defaultHigh
        }else{
            heightIphone.priority = .defaultHigh
            heightIpad.priority = .defaultLow
        }
        
        for i in 0...30{
            if i == 0{
                pickerData.append("Now")
            }else{
                pickerData.append("\(i)m ago")
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        typeChat.setTitle(dataTypeShow[FakeMessage_ChooseType], for: .normal)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .go{
            textField.resignFirstResponder()
            UIView.animate(withDuration: 0.5) {
                self.viewMain.frame = self.hold
            }
        }
        return true
    }
    
    @IBAction func moveViewMain(_ sender: Any) {
        self.bottomPickerView.constant = self.pickerView.frame.size.height
        UIView.animate(withDuration: 0.5) {
            self.viewMain.frame = self.move
            self.view.layoutIfNeeded()
            self.ViewOfPickerView.alpha = 0
        }
    }
    
    
    @IBAction func abtnCancel(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func abtnChooseChat(_ sender: Any) {
        
    }
    
    @IBAction func abtnChooseTime(_ sender: Any) {
        self.bottomPickerView.constant = 0
        self.nameTextField.resignFirstResponder()
        self.messageTextField.resignFirstResponder()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.viewMain.frame = self.move
            self.ViewOfPickerView.alpha = 1
        }
    }
    
    @IBAction func tapOverlay(_ sender: UITapGestureRecognizer) {
        nameTextField.resignFirstResponder()
        messageTextField.resignFirstResponder()
        self.bottomPickerView.constant = self.pickerView.frame.size.height
        UIView.animate(withDuration: 0.5) {
            self.viewMain.frame = self.hold
            self.view.layoutIfNeeded()
            self.ViewOfPickerView.alpha = 0
        }
        
    }
    
    @IBAction func abtnDone(_ sender: Any) {
        self.bottomPickerView.constant = self.pickerView.frame.size.height
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.viewMain.frame = self.hold
            self.ViewOfPickerView.alpha = 0
            
        }
    }
    @IBAction func abtnSave(_ sender: Any) {
        if nameTextField.text == ""{
            showAlert(title: "Error", message: "You need to fill Name TextField")
        }else if messageTextField.text == ""{
            showAlert(title: "Error", message: "Message TextField can not empty")
        }else{
            self.save()
        }
    }
    
    func save(){
        if check(){
            FakeMessageHandleCoreData.share.saveData(id: String(FakeMessageHandleCoreData.share.getID()), name: nameTextField.text!, message: messageTextField.text!, icon: String(FakeMessage_ChooseType), time: String(rowData))
        }else{
            FakeMessageHandleCoreData.share.edit(id: (FakeMessage_Edit?.id)!, name: nameTextField.text!, message: messageTextField.text!, icon: String(FakeMessage_ChooseType), time: String(rowData))
        }
        self.navigationController?.popToRootViewController(animated: true)
        UserDefaults.standard.set(true, forKey: "FakeMessage_Interstitial_Save")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CreateAndEditController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        btnChooseTime.setTitle(pickerData[row], for: .normal)
        self.rowData = row
    }
}
