//
//  SettingController.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 16/04/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import UIKit
import MessageUI

class SettingController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var BannerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (self.view.viewWithTag(5) as? UIButton)?.isHidden = true
        (self.view.viewWithTag(6) as? UIButton)?.isHidden = true
        (self.view.viewWithTag(7))?.isHidden = true
        (self.view.viewWithTag(8))?.isHidden = true
        self.configView()
        

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
//        if (UserDefaults.standard.bool(forKey: "Purchase") == true){
//            (self.view.viewWithTag(5) as? UIButton)?.isHidden = true
//            (self.view.viewWithTag(6) as? UIButton)?.isHidden = true
//            (self.view.viewWithTag(7))?.isHidden = true
//            (self.view.viewWithTag(8))?.isHidden = true
//            if BannerView != nil{
//                BannerView.isHidden = true
//            }
//        }
    }
    
    
    func configView(){
        if UIScreen.main.bounds.width >= 768{
            self.view.viewWithTag(1)?.isHidden = true
            (self.view.viewWithTag(2) as? UIButton)?.contentHorizontalAlignment = .center
            (self.view.viewWithTag(3) as? UIButton)?.contentHorizontalAlignment = .center
            (self.view.viewWithTag(4) as? UIButton)?.contentHorizontalAlignment = .center
            (self.view.viewWithTag(5) as? UIButton)?.contentHorizontalAlignment = .center
            (self.view.viewWithTag(6) as? UIButton)?.contentHorizontalAlignment = .center
        }
    }
    
    // action Button
    
    @IBAction func abtnRateourapp(_ sender: Any) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(urlAppGuide, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(urlAppGuide)
        }
    }
    
    @IBAction func abtnSharetoyourfriends(_ sender: Any) {
        let textToShare = "FakeApp"
        
        if let myWebsite = NSURL(string: linkApp) {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = sender as? UIView
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func abtnFeedback(_ sender: Any) {
        if MFMailComposeViewController.canSendMail(){
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.setToRecipients([mail])
            composeVC.setSubject("FakeApp Feedback")
            composeVC.setMessageBody("Hey Bro! Here's my feedback.", isHTML: false)
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Warring", message: "Mail services are not available", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func abtnRemoveAds(_ sender: Any) {
       
    }
    @IBAction func abtnRestorepurchase(_ sender: Any) {
     
    }
    
    
    
    // action Button
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


// InApp Purchase

extension SettingController{
    
    private func check(){
        if (UserDefaults.standard.value(forKey: "Purchase") == nil){
            (self.view.viewWithTag(5) as? UIButton)?.isHidden = false
            (self.view.viewWithTag(6) as? UIButton)?.isHidden = false
            (self.view.viewWithTag(7))?.isHidden = false
            (self.view.viewWithTag(8))?.isHidden = false
        }
    }
}


