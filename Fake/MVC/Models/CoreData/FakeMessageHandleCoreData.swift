//
//  FakeMessageHandleCoreData.swift
//  Fake
//
//  Created by Bé Nhện Của Bé Thảo on 20/04/2018.
//  Copyright © 2018 NatsuSalamada. All rights reserved.
//

import UIKit
import CoreData
class FakeMessageHandleCoreData {
    static let share = FakeMessageHandleCoreData()
    
    func saveData(id:String,name:String,message:String,icon:String,time:String){
        let fakeMessageEntity = FakeMessageEntity(context: AppDelegate.context)
        fakeMessageEntity.icon = icon
        fakeMessageEntity.message = message
        fakeMessageEntity.name = name
        fakeMessageEntity.time = time
        fakeMessageEntity.id = id 
        AppDelegate.saveContext()
    }
    
    func getAllData()->[FakeMessageEntity]{
        let fetchRequest = NSFetchRequest<FakeMessageEntity>(entityName: "FakeMessageEntity")
        let result = try? AppDelegate.context.fetch(fetchRequest)
        guard let entity = result else {return []}
        return entity
    }
    func getID()->String{
        let fetchRequest = NSFetchRequest<FakeMessageEntity>(entityName: "FakeMessageEntity")
        let result = try? AppDelegate.context.fetch(fetchRequest)
        guard let entity = result?.last else {return "-1"}
        return String(Int(entity.id!)! + 1)
    }
    func edit(id:String,name:String,message:String,icon:String,time:String){
        let predicateID = NSPredicate(format: "id == %@", id)
        let fetchRequest = NSFetchRequest<FakeMessageEntity>(entityName: "FakeMessageEntity")
        fetchRequest.predicate = predicateID
        let result = try? AppDelegate.context.fetch(fetchRequest)
        guard let entity = result?.first else {return }
        entity.icon = icon
        entity.message = message
        entity.name = name
        entity.time = time
        AppDelegate.saveContext()
    }
    func delete(id:String){
        let predicateID = NSPredicate(format: "id == %@", id)
        let fetchRequest = NSFetchRequest<FakeMessageEntity>(entityName: "FakeMessageEntity")
        fetchRequest.predicate = predicateID
        let result = try? AppDelegate.context.fetch(fetchRequest)

        guard let entity = result?.first else {return }
        AppDelegate.deleteContext(object: entity)
        
    }
}
