//
//  FakeMessageEntity+CoreDataProperties.swift
//  
//
//  Created by Bé Nhện Của Bé Thảo on 20/04/2018.
//
//

import Foundation
import CoreData


extension FakeMessageEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FakeMessageEntity> {
        return NSFetchRequest<FakeMessageEntity>(entityName: "FakeMessageEntity")
    }

    @NSManaged public var icon: String?
    @NSManaged public var id: String?
    @NSManaged public var message: String?
    @NSManaged public var name: String?
    @NSManaged public var time: String?

}
