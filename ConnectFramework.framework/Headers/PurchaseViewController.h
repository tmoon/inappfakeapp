//
//  ConnectFramework
//
#import <UIKit/UIKit.h>

@interface PurchaseViewController : UIViewController

// This method creates a UIViewController
// You should present this View Controller as needed to lock out features
// The optional result handler is called when the screen is dismissed
// The two parameters are bool and string, which are didPurchase and productIdentifer
+(nonnull UIViewController*)createPurchaseViewControllerWithResultHandler:(nullable void (^)(bool,NSString* _Nullable ))didPurchase;

// Use this method to check if the user has purchased already, before creating a new payment screen
// You should also use this method to block paid features
+(BOOL)didPurchase;
@end
