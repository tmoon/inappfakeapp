//
// ConnectFramework
//
#import <Foundation/Foundation.h>

@interface ConnectAnalytics : NSObject
/*
 
 These functions can be used to log analytics events.
 
 For Example, if someone taps on a button:
 
 // objective-c
 [ConnectAnalytics logEventWithCategoryName:@"Buttons" withAction:@"Button 1 Tapped"];
 
 // swift
 ConnectAnalytics.logEvent(categoryName:"Buttons", action:"Button 1 Tapped")
 
*/

+(void)logEventWithCategoryName:(nonnull NSString*)categoryName withAction:(nonnull NSString*)action;


+(void)logEventWithCategoryName:(nonnull NSString*)categoryName withAction:(nonnull NSString*)action withLabel:(nonnull NSString*)label withValue:(nonnull NSNumber*)value;

@end
